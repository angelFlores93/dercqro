google.load("visualization", "1", {packages:["corechart", 'bar']});
google.setOnLoadCallback(drawChart);
function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Año', 'Nacimiento', 'Reconocimiento', 'Adopción Simple', 'Matrimonios', 'Divorcios', 'Defunciones', 'Sentencias', 'Actas Foráneas', 'Promedio'],
        ['1997',  39520,732,39,10332,489,6033,88,0,7154],
        ['1998',  39113,728,61,9908,607,6261,82,0,7095],
        ['1999',  41007,825,58,10476,611,6103,74,0,7394],
        ['2000',  40337,898,30,10479,685,6198,80,0,7338],
        ['2001',  38132,847,10,9856,849,6288,78,0,7008],
        ['2002',  36933,856,7,9532,826,6425,96,0,6834],
        ['2003',  38688,858,63,9600,915,6731,139,0,7124],
        ['2004',  39612,919,40,9088,962,6674,199,0,7187],
        ['2005',  40985,1053,6,8785,1037,6874,215,0,7369],
        ['2006',  38863,1031,28,9023,1063,6954,164,0,7141],
        ['2007',  42757,1226,0,9318,1177,7169,3,0,8596],
        ['2008',  41310,1268,1,8842,1224,7676,1,0,7540],
        ['2009',  41155,1227,4,8185,1136,7757,5,0,7434],
        ['2010',  40867,1258,3,8268,1200,8250,3,330,7522],
        ['2011',  41760,1314,2,8977,1356,8077,5,987,7810],
        ['2012',  41553,1386,4,9240,1400,8622,7,782,7874],
        ['2013',  40953,1415,0,9726,1603,9107,11,622,7930],
        ['2014',  40981,1305,0,9713,1516,9349,5,619,7936],
        ['2015',  40755,1241,0,9461,1894,9284,9,588,7904],
    ]);
    var options = {
    
        titleTextStyle: {color:'white'},
        backgroundColor: {fill: 'transparent'},
        chartArea: {width:'85%',height:'65%'},
        vAxis: {textStyle:{color:'white'}},
        legend:{textStyle: {color: 'white'}, position: 'bottom'},
        height: 350,
        hAxis: {textStyle:{color:'white'}},
        seriesType: 'bars',
        series: {8 : {type: 'line'}},
        isStacked: true,
        explorer:  { actions: ['dragToZoom', 'rightClickToReset'] } ,
        tooltip: {trigger:'focus'}
    };
    var options2 = {
    
        titleTextStyle: {color:'white'},
        backgroundColor: {fill: 'transparent'},
        chartArea: {width:'85%',height:'65%'},
        vAxis: {textStyle:{color:'white'}},
        legend:{textStyle: {color: 'white'}, position: 'bottom'},
        height: 350,
        hAxis: {textStyle:{color:'white'}},
        seriesType: 'bars',
        series: {8 : {type: 'line'}},
       
    };
    var options3 = {
    
        titleTextStyle: {color:'white'},
        backgroundColor: {fill: 'transparent'},
        chartArea: {width:'85%',height:'65%'},
        vAxis: {textStyle:{color:'white'}},
        legend:{textStyle: {color: 'white'}, position: 'in'},
        height: 350,
        hAxis: {textStyle:{color:'white'}},
        seriesType: 'bars',
        series: {1 : {type: 'line'}},
        explorer:  { actions: ['dragToZoom', 'rightClickToReset'] } ,
       
    };
    var data2 = google.visualization.arrayToDataTable([
        ['Mes', 'Nacimiento', 'Defunciones', 'Matrimonios', 'Divorcios', 'Reconocimientos', 'Adopción Simple', 'Sentencias', 'Actas Foráneas', 'Promedio'],
        ['Ene-15',3865,897,586,130,138,0,0,38,807],
        ['Feb-15',3322,793,818,166,143,0,0,62,757],
        ['Mar-15',3314,802,745,156,88,0,0,52,736],
        ['Abr-15',3289,714,653,159,106,0,1,35,709],
        ['May-15',3153,718,662,155,20,0,0,37,686],
        ['Jun-15',3349,728,901,162,103,0,0,42,755]
        
    ]);
    var data3 = google.visualization.arrayToDataTable([
        
        ['Mes', 'Nacimiento', 'Defunciones', 'Matrimonios', 'Divorcios', 'Reconocimientos', 'Adopción Simple', 'Sentencias', 'Actas Foráneas', 'Promedio'],
        ['Jul-15',3254,697,797,168,10,0,2,70,727],
        ['Ago-15',3462,736,1182,131,123,0,0,62,813],
        ['Sep-15',3865,897,586,130,138,0,0,38,780],
        ['Oct-15',3322,793,818,166,143,0,0,62,727],
        ['Nov-15',3314,802,745,156,88,0,0,52,753],
        ['Dic-15',3289,714,653,159,106,0,1,35,777]
        
    ]);
    var data4 = google.visualization.arrayToDataTable([
        
        ['Municipio', 'Total de actos', 'Promedio por mes'],
        ['Amealco',1113,185],
        ['Pinal de Amoles',410,68],
        ['Arroyo Seco',189,31],
        ['Cadereyta',1128,188],
        ['Colón',945,157],
        ['Corregidora',1883,313],
        ['Ezequiel Montes',664,110],
        ['Huimilpan',558,93],
        ['Jalpan',447,74],
        ['Landa de Matamoros',299,49],
        ['El Marqués',2145,357],
        ['Pedro Escobedo',1141,190],
        ['Peñamiller',299,49],
        ['Querétaro',14175,2363],
        ['San Joaquín',122,20],
        ['San Juan del Río',4148,691],
        ['Tequisquiapan',1001,166],
        ['Tolimán',388,64],
        ['Oficialía DERQRO', 144,19]
        
    ]);
    var data5 = google.visualization.arrayToDataTable([
        
        ['Municipio', 'Total de actos', 'Promedio por mes'],
        ['Amealco',1138,113.8],
        ['Pinal de Amoles',422,42.2],
        ['Arroyo Seco',219,21.9],
        ['Cadereyta',1079,107.9],
        ['Colón',994,99.4],
        ['Corregidora',2068,206.8],
        ['Ezequiel Montes',692,69.2],
        ['Huimilpan',554,55.4],
        ['Jalpan',483,48.3],
        ['Landa de Matamoros',274,27.4],
        ['El Marqués',2144,214.4],
        ['Pedro Escobedo',951,95.1],
        ['Peñamiller',339,33.9],
        ['Querétaro',15059,1505.9],
        ['San Joaquín',170,17],
        ['San Juan del Río',3977,397.7],
        ['Tequisquiapan',975,97.5],
        ['Tolimán',487,48.7],
        ['Oficialía DERQRO', 61,6.1]
        
    ]);
    var chart = new google.visualization.ComboChart(document.getElementById('anual-bar-chart'));
    chart.draw(data, options);
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href") // activated tab
        if (target == '#anuales'){
            var chart = new google.visualization.ComboChart(document.getElementById('anual-bar-chart'));
            chart.draw(data, options);
        }
        else if (target == '#semestre-1'){
            var chart2 = new google.visualization.ComboChart(document.getElementById('semester-1-chart'));
            chart2.draw(data2, options2);
        }
        else if (target == '#semestre-2'){
            var chart3 = new google.visualization.ComboChart(document.getElementById('semester-2-chart'));
            chart3.draw(data3, options2);
        }
        else if (target == '#municipio-1'){
            var chart4 = new google.visualization.ComboChart(document.getElementById('provinces-chart-1'));
            chart4.draw(data4, options3);
        }
        else if (target == '#municipio-2'){
            var chart5 = new google.visualization.ComboChart(document.getElementById('provinces-chart-2'));
            chart5.draw(data5, options3);
        }
    });
}
